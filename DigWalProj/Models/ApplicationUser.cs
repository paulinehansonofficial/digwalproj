using System;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

public class ApplicationUser : IdentityUser
{

    // userID
    // firstName
    // lastName

    // how to manage phoneNumber?
    // it's a field in IdentityUser, but we don't want the teacher to have to give the phone number for every student when creating the account

}